# Scoper config for Twig v.3 

## What is it?

Example of the [Scoper](https://github.com/humbug/php-scoper) configuration for Twig v3.

It includes a series of patches to address issues arising from
Twig's [global functions](https://github.com/twigphp/Twig/issues/3645), which cause problems by default.

This configuration is based on [Twig Scoper v1](https://github.com/OnTheGoSystems/twig-scoper). Many thanks to the
author of that package.

## How to use?

Copy the content of `scoper.inc.php` into your scoper configuration and merge the return (final) section.

Remember to replace "fixme" with your desired namespace.